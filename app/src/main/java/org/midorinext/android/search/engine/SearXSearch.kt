package org.midorinext.android.search.engine

import org.midorinext.android.R

class SearXSearch: BaseSearchEngine(
        "file:///android_asset/searx.webp",
        "https://searx.privatenet.cf/?q=",
        R.string.search_engine_searx
)