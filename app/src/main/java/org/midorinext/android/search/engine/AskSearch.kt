package org.midorinext.android.search.engine

import org.midorinext.android.R

/**
 * The Ask search engine.
 */
class AskSearch : BaseSearchEngine(
    "file:///android_asset/ask.webp",
    "http://www.ask.com/web?qsrc=0&o=0&l=dir&qo=MidoriBrowser&q=",
    R.string.search_engine_ask
)
