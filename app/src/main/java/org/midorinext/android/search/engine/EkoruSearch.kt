package org.midorinext.android.search.engine

import org.midorinext.android.R

/**
 * The Baidu search engine.
 *
 * See http://www.baidu.com/img/bdlogo.gif for the icon.
 */
class EkoruSearch : BaseSearchEngine(
    "file:///android_asset/ekoru.webp",
    "https://www.ekoru.org/?ext=midoribrowser&q=",
    R.string.ekoru
)
