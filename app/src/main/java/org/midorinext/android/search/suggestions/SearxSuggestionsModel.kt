package org.midorinext.android.search.suggestions

import android.app.Application
import io.reactivex.Single
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import org.midorinext.android.constant.UTF8
import org.midorinext.android.database.SearchSuggestion
import org.midorinext.android.extensions.preferredLocale
import org.midorinext.android.log.Logger
import org.midorinext.android.preference.UserPreferences

class SearxSuggestionsModel(
        okHttpClient: Single<OkHttpClient>,
        requestFactory: RequestFactory,
        application: Application,
        logger: Logger,
        userPreferences: UserPreferences
) : BaseSuggestionsModel(okHttpClient, requestFactory, UTF8, application.preferredLocale, logger, userPreferences) {

    override fun createQueryUrl(query: String, language: String): HttpUrl {
        TODO("Not yet implemented")
    }

    override fun parseResults(responseBody: ResponseBody): List<SearchSuggestion> {
        TODO("Not yet implemented")
    }
}