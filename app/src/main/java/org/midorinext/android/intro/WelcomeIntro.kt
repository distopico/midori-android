package org.midorinext.android.intro

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import androidx.preference.PreferenceManager
import android.util.Log
import androidx.fragment.app.Fragment
import com.github.appintro.*
import org.midorinext.android.BrowserApp
import org.midorinext.android.MainActivity
import org.midorinext.android.R

class WelcomeIntro : AppIntro2(){

    lateinit var preference : SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preference = applicationContext.getSharedPreferences(PREFERENCE_CONFIGURATION_NAME, PRIVATE_MODE)
        if (!preference.getBoolean(FIRST_TIME, false)){
            showSlide()
            //addSlide(AppIntroCustomLayoutFragment.newInstance(R.layout.activity_login_astian))
            //showSlideCustom()
        }
    }

    private fun showSlide(){
        // Call addSlide passing your Fragments.
        // You can use AppIntroFragment to use a pre-built fragment
        addSlide(AppIntroFragment.newInstance(
                title = "Welcome Midori Next - Halanna",
                backgroundColor = Color.parseColor("#12a90b"),
                description = "A lightweight, fast, secure, open source web browser"
        ))
        addSlide(AppIntroFragment.newInstance(
                title = "Choose your favorite search engine!",
                backgroundColor = Color.parseColor("#12a90b"),
                description = "Customize your search engine"
        ))

        setIndicatorColor(
                selectedIndicatorColor = (Color.GREEN),
                unselectedIndicatorColor = (Color.RED)
        )
    }
    /***
    private fun showSlideCustom(){
        addSlide(AppIntroCustomLayoutFragment.newInstance(R.layout.activity_login_astian))
    }*/

    private fun main()
    {
        startActivity(Intent(this, MainActivity::class.java))
    }

    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        // Decide what to do when the user clicks on "Skip"
        main()
        finish()
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        // Decide what to do when the user clicks on "Done"
        main()
        finish()
    }

    override fun onSlideChanged(oldFragment: Fragment?, newFragment: Fragment?) {
        super.onSlideChanged(oldFragment, newFragment)
        Log.d("Hello", "Changed")
    }

    override fun onIntroFinished() {
        super.onIntroFinished()
        /**preference = applicationContext.getSharedPreferences(PREFERENCE_CONFIGURATION_NAME, PRIVATE_MODE)
        preference.getBoolean(FIRST_TIME, false)
         */
        PreferenceManager.getDefaultSharedPreferences(this).edit().apply() {
            putBoolean(FIRST_TIME, true)
            apply()
        }
    }

    companion object {
        private const val PRIVATE_MODE = 0
        private const val PREFERENCE_CONFIGURATION_NAME = "configuration"
        private const val FIRST_TIME = "isFirstRun"
    }
}