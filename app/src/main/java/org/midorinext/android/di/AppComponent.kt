package org.midorinext.android.di

import org.midorinext.android.BrowserApp
import org.midorinext.android.adblock.BloomFilterAdBlocker
import org.midorinext.android.adblock.NoOpAdBlocker
import org.midorinext.android.browser.SearchBoxModel
import org.midorinext.android.browser.activity.BrowserActivity
import org.midorinext.android.browser.activity.ThemableBrowserActivity
import org.midorinext.android.browser.bookmarks.BookmarksDrawerView
import org.midorinext.android.device.BuildInfo
import org.midorinext.android.dialog.LightningDialogBuilder
import org.midorinext.android.download.DownloadHandler
import org.midorinext.android.download.LightningDownloadListener
import org.midorinext.android.reading.activity.ReadingActivity
import org.midorinext.android.search.SuggestionsAdapter
import org.midorinext.android.settings.activity.SettingsActivity
import org.midorinext.android.settings.activity.ThemableSettingsActivity
import org.midorinext.android.settings.fragment.*
import org.midorinext.android.utils.ProxyUtils
import org.midorinext.android.view.MidoriNextChromeClient
import org.midorinext.android.view.MidoriNextView
import org.midorinext.android.view.MidoriNextWebClient
import android.app.Application
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class), (AppBindsModule::class)])
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        @BindsInstance
        fun buildInfo(buildInfo: BuildInfo): Builder

        fun build(): AppComponent
    }

    fun inject(activity: BrowserActivity)

    fun inject(fragment: BookmarkSettingsFragment)

    fun inject(builder: LightningDialogBuilder)

    fun inject(midoriNextView: MidoriNextView)

    fun inject(activity: ThemableBrowserActivity)

    fun inject(advancedSettingsFragment: AdvancedSettingsFragment)

    fun inject(app: BrowserApp)

    fun inject(activity: ReadingActivity)

    fun inject(webClient: MidoriNextWebClient)

    fun inject(activity: SettingsActivity)

    fun inject(activity: ThemableSettingsActivity)

    fun inject(listener: LightningDownloadListener)

    fun inject(fragment: PrivacySettingsFragment)

    fun inject(fragment: DebugSettingsFragment)

    fun inject(fragment: ExtensionsSettingsFragment)

    fun inject(suggestionsAdapter: SuggestionsAdapter)

    fun inject(chromeClient: MidoriNextChromeClient)

    fun inject(searchBoxModel: SearchBoxModel)

    fun inject(generalSettingsFragment: GeneralSettingsFragment)

    fun inject(displaySettingsFragment: DisplaySettingsFragment)

    fun inject(adBlockSettingsFragment: AdBlockSettingsFragment)

    fun inject(parentalSettingsFragment: ParentalControlSettingsFragment)

    fun inject(bookmarksView: BookmarksDrawerView)

    fun provideBloomFilterAdBlocker(): BloomFilterAdBlocker

    fun provideNoOpAdBlocker(): NoOpAdBlocker

}
