package org.midorinext.android.sync

import android.app.Application
import com.parse.Parse
import com.parse.ParseInstallation
import org.midorinext.android.R

class AppSync : Application() {
    override fun onCreate() {
        super.onCreate()
        Parse.initialize(Parse.Configuration.Builder(this)
                .applicationId(getString(R.string.astian_app_id))
                .clientKey(getString(R.string.astian_client_key))
                .server(getString(R.string.astian_url_back))
                .build()
        )
        ParseInstallation.getCurrentInstallation().saveInBackground()
    }
}