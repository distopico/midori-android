package org.midorinext.android.sync

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.parse.ParseUser
import org.midorinext.android.R

class ResetPasswordActivity : AppCompatActivity() {
    var edEmail: EditText? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)
        edEmail = findViewById(R.id.edEmail)
    }

    fun resetPassword(view: View?) {
        if (TextUtils.isEmpty(edEmail!!.text)) {
            edEmail!!.error = "Email is required!"
        } else {
            ParseUser.requestPasswordResetInBackground(edEmail!!.text.toString()) { e ->
                if (e == null) {
                    // An email was successfully sent with reset instructions.
                    Toast.makeText(this@ResetPasswordActivity, "An email was successfully sent with reset instructions.", Toast.LENGTH_LONG).show()
                } else {
                    // Something went wrong. Look at the ParseException to see what's up.
                    Toast.makeText(this@ResetPasswordActivity, "Something went wrong. Look at the ParseException to see what's up.", Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}