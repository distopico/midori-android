package org.midorinext.android.sync

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.parse.ParseUser
import org.midorinext.android.R

class SignupActivity : AppCompatActivity() {
    var edName: EditText? = null
    var edEmail: EditText? = null
    var edPassword: EditText? = null
    var edConfirmPassword: EditText? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        edName = findViewById(R.id.edName)
        edEmail = findViewById(R.id.edEmail)
        edPassword = findViewById(R.id.edPassword)
        edConfirmPassword = findViewById(R.id.edConfirmPassword)
    }

    fun signup(view: View?) {
        if (TextUtils.isEmpty(edName!!.text)) {
            edName!!.error = "Name is required!"
        } else if (TextUtils.isEmpty(edEmail!!.text)) {
            edEmail!!.error = "Email is required!"
        } else if (TextUtils.isEmpty(edPassword!!.text)) {
            edPassword!!.error = "Password is required!"
        } else if (TextUtils.isEmpty(edConfirmPassword!!.text)) {
            edConfirmPassword!!.error = "Confirm password is required!"
        } else if (edPassword!!.text.toString() != edConfirmPassword!!.text.toString()) {
            Toast.makeText(this@SignupActivity, "Passwords are not the same!", Toast.LENGTH_LONG).show()
        } else {
            val progress = ProgressDialog(this)
            progress.setMessage("Loading ...")
            progress.show()
            val user = ParseUser()
            user.username = edEmail!!.text.toString().trim { it <= ' ' }
            user.email = edEmail!!.text.toString().trim { it <= ' ' }
            user.setPassword(edPassword!!.text.toString())
            user.put("name", edName!!.text.toString().trim { it <= ' ' })
            user.signUpInBackground { e ->
                progress.dismiss()
                if (e == null) {
                    Toast.makeText(this@SignupActivity, "Welcome!", Toast.LENGTH_LONG).show()
                    val intent = Intent(this@SignupActivity, HomeActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    ParseUser.logOut()
                    Toast.makeText(this@SignupActivity, e.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}